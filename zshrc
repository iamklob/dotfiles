# vim: filetype=zsh
# see http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Keymaps
bindkey -e
bindkey "^[[3~" delete-char
bindkey "^[f" forward-word
bindkey "^[b" backward-word
bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word


export HISTSIZE=32768
export SAVEHIST=$HISTSIZE
export HISTFILESIZE=$HISTSIZE
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help"
# original WORDCHARS="*?_-.[]~=/&;!#$%^(){}<>" (includes /)
export WORDCHARS="*?_-.[]~=&;!#$%^(){}<>"

# ZSH opts
setopt NO_CASE_GLOB
setopt AUTO_CD
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_REDUCE_BLANKS
setopt CORRECT
setopt CORRECT_ALL
setopt HIST_IGNORE_SPACE

# locale - setup first - do not move to `export`
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

autoload -Uz compinit
if [ $(date +'%j') != $(stat -f '%Sm' -t '%j' ~/.zcompdump) ]; then
  compinit
else
  compinit -C
fi

# load children
for file in ~/.{path,exports,aliases}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file"
done
unset file

if command -v starship >/dev/null 2>&1; then
    eval "$(starship init zsh)"
else
    # setup git branch in prompt
    export PS1='\h:\W$(__git_ps1 " (\[\033[01;32m\]%s\[\033[00m\])") \u\$ '
fi

# use pyenv
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

if command -v zoxide >/dev/null 2>&1; then
    eval "$(zoxide init zsh)"
fi

if command -v fnm >/dev/null 2>&1; then
    eval "$(fnm env)"
fi

if [ Darwin = `uname` ]; then
    # use lesspipe
    export LESSOPEN="|/usr/local/bin/lesspipe.sh %s" LESS_ADVANCED_PREPROCESSOR=1
fi
