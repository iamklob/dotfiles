# vim: filetype=zsh
if ls --color > /dev/null 2>&1; then # GNU
    flag="--color"
else # BSD
    flag="-G"
fi

export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:'
alias ls="command ls ${flag}"
alias ll="ls -l"


alias be='bundle exec'
alias g='git'
# for completion on g alias for git
#complete -o default -o nospace -F __git_complete g _git_main

alias key2clip='cat ~/.ssh/id_rsa.pub | pbcopy'
alias open_payroll='pdfcrack -m 4 -n 4 -charset="1234567890"'
alias dl="cd ~/Downloads"
alias dnsflush='sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder'
alias tcplist='lsof -iTCP -sTCP:LISTEN -n -P'
alias udplist='lsof -iUDP -n -P'
alias pub_ip='curl ifconfig.me/ip'

# util
# previous-last btc block hash
lbtcbh () {
    height=$(curl --silent https://blockchain.info/latestblock | jq .height | tr -d "\\n\"")
    height=$((height-1))
    hash=$(curl --silent https://blockchain.info/block-height/$height?format=json | jq .blocks[0].hash | tr -d "\\n\"")
    echo -n "$hash"
}

# URL-encode strings
alias urlencode='python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1]);"'

# Merge PDF files
# Usage: `mergepdf -o output.pdf input{1,2,3}.pdf`
alias mergepdf='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py'

# spotify
alias spl='spotify play'
alias spa='spotify pause'
alias sps='spotify status'

alias alert='terminal-notifier -title "$([ $? = 0  ] && echo Terminal || echo Error)" -message "$(history|tail -n1|sed -E "s/^\s*[0-9 ]+\s*//; s/[\;\&\|\s]*(alert)$//")"'

# cd aliases
alias cdw='cd ~/Work'
alias cdp='cd ~/Prog'

# iTea aliases
alias itea_start_tomcat='JAVA_OPTS="-Dsun.lang.ClassLoader.allowArraySyntax=true -Dorg.apache.commons.logging.Log=org.apache.commons.logging.impl.Jdk14Logger" /usr/local/Cellar/tomcat6/6.0.41/libexec/bin/startup.sh'
alias itea_stop_tomcat='JAVA_OPTS="-Dsun.lang.ClassLoader.allowArraySyntax=true -Dorg.apache.commons.logging.Log=org.apache.commons.logging.impl.Jdk14Logger" /usr/local/Cellar/tomcat6/6.0.41/libexec/bin/shutdown.sh'

# python
alias ptww='pytest-watch --onpass "terminal-notifier -message \"All tests passed\" -appIcon ~/dotfiles/notify-icons/Success.icns" --onfail "terminal-notifier -message \"Tests failed\" -appIcon ~/dotfiles/notify-icons/Failed.icns" -- -vv '

pyclean () {
        find . -type f -name "*.py[co]" -delete
        find . -type d -name "__pycache__" -delete
}

vg () {
    # Run commands inside Vagrant.
    vagrant ssh -t -c "$*"
}

ansi2txt () {
    expr="s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g"
    if (( $# == 0 )) ; then
        gsed -r $expr
    else
        gsed -r $expr $@
    fi
}

image64 (){
    filename=$(basename $1)
    ext=${filename##*.}
    if [ $ext == gif ]; then
        append="data:image/gif;base64,";
    elif [ $ext == jpeg ] || [ $ext == jpg ]; then
        append="data:image/jpeg;base64,";
    elif [ $ext == png ]; then
        append="data:image/png;base64,";
        echo "this worked $1"
    elif [ $ext == svg ]; then
        append="data:image/svg+xml;base64,";
    elif [ $ext == ico ]; then
        append="data:image/vnd.microsoft.icon;base64,";
    fi

    #Mathias Bynens - http://superuser.com/questions/120796/os-x-base64-encode-via-command-line
    data=$(openssl base64 < $1 | tr -d '\n')

    if [ "$#" -eq 2 ] && [ $2 == -img ]; then
        data=\<img\ src\=\"$append$data\"\>
    else
        data=$append$data
    fi

    echo $data | pbcopy

    echo "copied to clipboard"
}

alias torrent_d_up='transmission-daemon -w ~/Downloads/torrents -x ~/Downloads/transmission.pid -f'
alias torrent_d_down='kill $(cat ~/Downloads/transmission.pid)'
