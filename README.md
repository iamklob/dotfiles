# dotfiles

## Mac

- run `./makesymlinks.sh`
- install homebrew `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
- run `brew bundle` in the dotfiles directory
- run `cargo install cargo-edit cargo-check cargo-embed cargo-binutils cargo-flash cargo-ssearch cargo-watch cargo-outdated alert-after cargo-lichking cargo-expand`
