" vim: ts=2 sw=2 et
set nocompatible               " be iMproved
filetype off                   " required!
set number
set guifont=Menlo\ Regular:h12

" use system clipboard on Mac
" doesn't work with system vim
" (brew macvim)
set clipboard=unnamed
set ruler                 " Always show info along bottom.
set autoindent            " auto-indent
set tabstop=4             " tab spacing
set softtabstop=4         " unify
set shiftwidth=4          " indent/outdent by 4 columns
set shiftround            " always indent/outdent to the nearest tabstop
set expandtab             " use spaces instead of tabs
set smarttab              " use tabs at the start of a line, spaces elsewhere
set nowrap                " don't wrap text
set hls                   " highlight search
set noswapfile            " no backups, we have git
set autoread              " reload files
set nofoldenable          " no folding at all
set backspace=indent,eol,start


" search
set ignorecase
set smartcase

" highlight current line
set cul

" status line (github.com/voy/dotfiles)
set ruler
set laststatus=2

set statusline=%(%m\ %)pathshorten(%f)%(\ %y%)%(\ [%{&fileencoding}]%)\ %{fugitive#statusline()}%=[%3b,%4(0x%B%)]\ %3c\ %4l\ /%5L\ %4P
set statusline+=%#warningmsg#
set statusline+=%*

set showcmd
set mouse=a

set t_Co=256

" visualize leading tab and trailing whitespace
set list lcs=tab\:\·\ ,trail:·

" pastetoggle
set pastetoggle=<leader>l

" remove trailing whitespace before save
autocmd BufWritePre * :%s/\s\+$//e

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required!
Plugin 'VundleVim/Vundle.vim'

" My Plugins here:
" original repos on github
Plugin 'tpope/vim-fugitive'
Plugin 'plasticboy/vim-markdown'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'chriskempson/base16-vim'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'bling/vim-airline'
Plugin 'jeffkreeftmeijer/vim-numbertoggle'
Plugin 'mxw/vim-jsx'
Plugin 'pangloss/vim-javascript'
Plugin 'tomtom/tcomment_vim'
" Plugin 'SirVer/ultisnips'
" Plugin 'honza/vim-snippets'
" Plugin 'StanAngeloff/php.vim'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'w0rp/ale'
Plugin 'hdima/python-syntax'
Plugin 'tpope/vim-surround'
Plugin 'rking/ag.vim'
Plugin 'elixir-editors/vim-elixir'
" Plugin 'fatih/vim-go'
Plugin 'leafgarland/typescript-vim'
Plugin 'rust-lang/rust.vim'
Plugin 'tikhomirov/vim-glsl'
Plugin 'DingDean/wgsl.vim'
Plugin 'kaarmu/typst.vim'
" vim-scripts repos
" Plugin 'L9'
" non github repos
Plugin 'elmcast/elm-vim'
Plugin 'cespare/vim-toml'
Plugin 'junegunn/vim-easy-align'

call vundle#end()             " required!
filetype plugin indent on     " required!
syntax enable
"
" Brief help
" :PluginList          - list configured bundles
" :PluginInstall(!)    - install(update) bundles
" :PluginSearch(!) foo - search(or refresh cache first) for foo
" :PluginClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Plugin command are not allowed..

" theme and background
let g:base16colorspace=256
set background=dark
colorscheme base16-tomorrow-night

let g:elm_setup_keybindings = 0
" Copy path to clipboard
" Convert slashes to backslashes for Windows.
if has('win32')
  nmap ,cs :let @*=substitute(expand("%"), "/", "\\", "g")<CR>
  nmap ,cl :let @*=substitute(expand("%:p"), "/", "\\", "g")<CR>

  " This will copy the path in 8.3 short format, for DOS and Windows 9x
  nmap ,c8 :let @*=substitute(expand("%:p:8"), "/", "\\", "g")<CR>
else
  nmap ,cs :let @*=expand("%")<CR>
  "nmap ,cl :let @*=expand("%:p")<CR>
  nmap ,cl :let @*=substitute(expand("%:p"), getcwd()."/", "", "g")<CR>
endif

" Clojure
au BufRead,BufNewFile *.clj set filetype=clojure
au Syntax clojure RainbowParenthesesActivate
au Syntax clojure RainbowParenthesesLoadRound

" Ack
let g:ackprg = 'rg --vimgrep --no-heading'
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always --ignore-case '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)

nnoremap <leader>r :Rg

" Markdown syntax
au BufRead,BufNewFile *.md,*.mdown set filetype=markdown

" OpenCL -> C syntax
au BufRead,BufNewFile *.cl set syntax=c

" Make
autocmd filetype make setlocal noexpandtab

" Commit msgs
autocmd Filetype gitcommit setlocal spell textwidth=72

" Fzf
function! s:GotoOrOpen(command, ...)
  for file in a:000
    if a:command == 'e'
      exec 'e ' . file
    else
      exec "tab drop " . file
    endif
  endfor
endfunction

command! -nargs=+ GotoOrOpen call s:GotoOrOpen(<f-args>)
let g:fzf_buffers_jump = 1
let g:fzf_action = {
    \ '': 'GotoOrOpen tab',
    \ 'ctrl-t': 'tab drop',
    \ 'ctrl-s': 'split',
    \ 'ctrl-v': 'vsplit',
    \ }

let g:fzf_command_prefix = 'Fzf'
" let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true, 'yoffset': 1.0 } }
let g:fzf_layout = { 'down': 12 }
nnoremap <leader>t :FzfGFiles<CR>
nnoremap <leader>f :FzfFiles<CR>
nnoremap <leader>b :FzfBuffer<CR>

" Setup Airline
let g:airline_powerline_fonts = 1
let g:airline_mode_map = {
    \ '__' : '-',
    \ 'n'  : 'N',
    \ 'i'  : 'I',
    \ 'R'  : 'R',
    \ 'c'  : 'C',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ }
let g:airline_theme='base16_tomorrow'
" let g:airline_section_c = '%<%{pathshorten(substitute(expand("%:p"), getcwd()."/", "", "g"))}%m%#__accent_red#%{airline#util#wrap(airline#parts#readonly(),0)}%#__restore__#'
" let g:airline_section_y = '%{airline#util#wrap(airline#parts#ffenc(),0)}%{&expandtab?"[spc]":"[tab]"}'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0

"" Setup ale
let g:ale_sign_error = "✖"
let g:ale_sign_warning = "⚠"
let g:ale_sign_info = "i"
let g:ale_sign_hint = "➤"

let g:ale_echo_msg_error_str = '✗'
let g:ale_echo_msg_warning_str = '!'
let g:ale_echo_msg_format = '[%linter%] %s'
let g:ale_completion_enabled = 1

let g:ale_floating_preview = 1

" fix on save of file for fixers defined below
let g:ale_fix_on_save = 1

let g:ale_linters = {
\   'javascript': [ 'eslint', 'flow' ],
\   'rust': [ 'analyzer' ],
\   'python': [ 'black', 'pylsp' ],
\   'glsl':  [ 'glslangValidator' ],
\   'elixir': [ 'credo', 'mix', 'elixir_ls' ]
\}
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'rust': [ 'rustfmt' ],
\   'python': [ 'black' ],
\   'elixir': [ 'mix_format' ],
\   'javascript': [ 'eslint' ]
\}

let g:ale_pattern_options = {
\   'bcnode/src': {
\       'ale_fixers': { 'rust': [ 'rustfmt' ] }
\   }
\}

let g:ale_rust_rustfmt_options = '--edition=2021'
" see https://rust-analyzer.github.io/manual.html#configuration for more
let g:ale_rust_analyzer_config = {
\ 'cargo': {
    \ 'autoreload': 'true',
    \ }
\ }
let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
let g:ale_python_pylsp_auto_poetry = 1
let g:ale_python_black_auto_poetry = 1

nnoremap <leader>ad :ALEDisableBuffer<CR>
nnoremap <leader>aD :ALEDisable<CR>
nnoremap <leader>af :ALEFix
nnoremap <leader>ac :ALECodeAction<CR>
nnoremap <leader>ar :ALERename<CR>
nnoremap <leader>agd :ALEGoToDefinition<CR>
nnoremap K :ALEHover<CR>

" UtilSnips setup
" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" do not fold markdown sections
let g:vim_markdown_folding_disabled = 1

"  Parentheses colours using Solarized
let g:rbpt_colorpairs = [
  \ [ '13', '#6c71c4'],
  \ [ '5',  '#d33682'],
  \ [ '1',  '#dc322f'],
  \ [ '9',  '#cb4b16'],
  \ [ '3',  '#b58900'],
  \ [ '2',  '#859900'],
  \ [ '6',  '#2aa198'],
  \ [ '4',  '#268bd2'],
  \ ]

" Clear search highlight
nnoremap <silent> _ :nohl<CR>

" JSX in JS
let g:jsx_ext_required = 0

" Javascript
let g:javascript_plugin_flow = 1

let g:EditorConfig_exec_path = '/opt/homebrew/bin/editorconfig'

let g:ctrlp_buffer_func = { 'enter': 'CtrlPMappings' }

" Change color of tab segment for solarized
" hi SpecialKey ctermbg=8 ctermfg=10

function! CtrlPMappings()
  nnoremap <buffer> <silent> <C-@> :call <sid>DeleteBuffer()<cr>
endfunction

function! s:DeleteBuffer()
  let path = fnamemodify(getline('.')[2:], ':p')
  let bufn = matchstr(path, '\v\d+\ze\*No Name')
  exec "bd" bufn ==# "" ? path : bufn
  exec "norm \<F5>"
endfunction

let python_hightlight_all = 1

" augroup python_files
"     autocmd FileType python setlocal noexpandtab
"     autocmd FileType python set tabstop=4
"     autocmd FileType python set shiftwidth=4
"     autocmd FileType python set list lcs=tab\:\·\ ,trail:·
" augroup END
"
" augroup coffee_script
"     autocmd FileType coffee setlocal noexpandtab
"     autocmd FileType coffee set tabstop=4
"     autocmd FileType coffee set shiftwidth=4
"     autocmd FileType coffee set list lcs=tab\:\·\ ,trail:·
" augroup END

augroup yaml_files
    autocmd FileType yaml set tabstop=2
    autocmd FileType yaml set shiftwidth=2
augroup END

au BufRead,BufNewFile Dockerfile* setfiletype dockerfile
au BufNewFile,BufRead *.service* setfiletype systemd

augroup vimrc_todo
    au!
    au Syntax * syn match MyTodo /\v<(FIXME|NOTE|TODO|OPTIMIZE|XXX|HACK|BUG)/
                \ containedin=.*Comment,vimCommentTitle
augroup END
hi def link MyTodo Todo

au FileType markdown vmap <Leader>t :EasyAlign*<Bar><Enter>

" tab switching
nnoremap H gT
nnoremap L gt

" buffe closing
nnoremap <leader>w :bd <CR>
nnoremap <leader>W :bufdo bd <CR>

" Goodbye, ESC (https://goo.gl/JwNZNT)
inoremap jj <esc>
inoremap kk <esc>
" noremap <esc> <nop>
" noremap! <esc> <nop>
