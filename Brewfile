# Brewfile as described on https://github.com/mxcl/homebrew/pull/24107

# Install brew-cask subsystem
tap 'caskroom/cask'

# Install homebrew-bundle subsystem
tap 'Homebrew/bundle'

# Tap fonts for powerline font
tap 'caskroom/fonts'

# Install needed formulae
brew 'bat'
brew 'caddy'
brew 'curl'
brew 'dfu-util'
brew 'dos2unix'
brew 'editorconfig'
brew 'elixir'
brew 'fd'
brew 'ffsend'
brew 'fnm'
brew 'fswatch'
brew 'fzf'
brew 'git'
brew 'git-lfs'
brew 'htop'
brew 'hub'
brew 'imagemagick'
brew 'jq'
brew 'lesspipe'
brew 'mas'
brew 'mosh'
brew 'open-ocd'
brew 'pdfcrack'
brew 'pipenv'
brew 'postgresql'
brew 'protobuf'
brew 'pyenv'
brew 'python'
brew 'python3'
brew 'reattach-to-user-namespace'
brew 'redis'
brew 'rg'
brew 'rust-analyzer'
brew 'rustup-init'
brew 'shellcheck'
brew 'sqlite'
brew 'starship'
brew 'terminal-notifier'
brew 'tmux'
brew 'tokei'
brew 'tree'
brew 'unrar'
brew 'vim'
brew 'wget'
brew 'youtube-dl'
brew 'zoxide'

# casks
cask 'alacritty'
cask 'dash'
cask 'docker'
cask 'dropbox'
cask 'eloston-chromium'
cask 'firefox'
cask 'font-fira-code'
cask 'gpg-suite'
cask 'istat-menus'
cask 'qlmarkdown'
cask 'qlstephen'
cask 'signal'
cask 'sizeup'
cask 'telegram'
cask 'tor-browser'
cask 'zoomus'

# Mac app store
mas '1Password', id: 443987910
mas 'Be Focused Pro', id: 961632517
mas 'Things 3', id: 904280696
